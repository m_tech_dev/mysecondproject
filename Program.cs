﻿using System;
using MyLib2;

namespace MySecondProject
{
    class Program
    {
        static void Main(string[] args)
        {
            Person tom = new Person { name = "Tom", age = 35 };
            Console.WriteLine(tom.name);
        }
    }
}
